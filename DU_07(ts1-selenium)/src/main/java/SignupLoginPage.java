
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class SignupLoginPage {

    private final WebDriver driver;

    @FindBy(id = "login-box-email")
    private WebElement loginEmailField;

    @FindBy(id = "login-box-pw")
    private WebElement loginPasswordField;

    private WebElement loginFormSubmitButton;

    public SignupLoginPage(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);

        loginFormSubmitButton = driver.findElement(By.cssSelector(".btn[title='Log in']"));
    }

    public SignupLoginPage(WebDriver driver, String queryString) {
        this.driver = driver;
        driver.get(queryString);

        PageFactory.initElements(driver, this);

        // Close cookies
        try {
            driver.findElement(By.cssSelector("button[data-cc-action='accept']")).click();
        } catch (NoSuchElementException e) {}

        loginFormSubmitButton = driver.findElement(By.cssSelector(".btn[title='Log in']"));
    }

    public void setLoginEmail(String email) {
        this.loginEmailField.sendKeys(email);
    }

    public void setLoginPassword(String password) {
        this.loginPasswordField.sendKeys(password);
    }

    public void sendLoginForm() {
        loginFormSubmitButton.click();
    }
}

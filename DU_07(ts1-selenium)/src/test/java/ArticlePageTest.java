import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ArticlePageTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void getNameTest() throws InterruptedException {

        String expectedName = "APOGEN: automatic page object generator for web testing";

        ArticlePage articlePage = new ArticlePage(driver, "https://link.springer.com/article/10.1007/s11219-016-9331-9");
        Thread.sleep(2000);

        assertEquals(expectedName, articlePage.getName());
        driver.close();
    }

    @Test
    public void getDateTest() throws InterruptedException {

        String expectedDate = "09 August 2016";

        ArticlePage articlePage = new ArticlePage(driver, "https://link.springer.com/article/10.1007/s11219-016-9331-9");
        Thread.sleep(2000);

        assertEquals(expectedDate, articlePage.getDate());
        driver.close();
    }

    @Test
    public void getDOITest() throws InterruptedException {

        String expectedDOI = "https://doi.org/10.1007/s11219-016-9331-9";

        ArticlePage articlePage = new ArticlePage(driver, "https://link.springer.com/article/10.1007/s11219-016-9331-9");
        Thread.sleep(2000);

        assertEquals(expectedDOI, articlePage.getDOI());
        driver.close();
    }
}

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SearchPageTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void setTypeChapterTest() throws InterruptedException {

        String expectedValue = "within Chapter";

        SearchPage searchPage = new SearchPage(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        searchPage.setContentType("Chapter");
        Thread.sleep(2000);

        assertEquals(expectedValue, driver.findElement(By.cssSelector(".facet-constraint-message")).getText());
        driver.close();
    }

    @Test
    public void setPageTest() throws InterruptedException {

        String expectedValue = "5";

        SearchPage searchPage = new SearchPage(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        searchPage.setPage(5);
        Thread.sleep(2000);

        assertEquals(expectedValue, driver.findElement(By.cssSelector("input[name='go-to-page']")).getAttribute("value"));
        driver.close();
    }

    @Test
    public void getResultsLinksTest() throws InterruptedException {

        SearchPage searchPage = new SearchPage(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        Thread.sleep(500);

        assertEquals(4, searchPage.getResultsLinks(4).size());
        driver.close();
    }
}

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdvancedSearchPageTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void advancedSearchTest() throws InterruptedException {

        String expectedUrl = "https://link.springer.com/search?query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29&date-facet-mode=in&facet-start-year=2022&showAll=true";

        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.setAllWords("Page Object Model");
        advancedSearchPage.setLeastWords("Sellenium Testing");
        advancedSearchPage.setDateFacetModeToIn();
        advancedSearchPage.setFacetStartYear("2022");
        Thread.sleep(2000);
        advancedSearchPage.sendForm();

        assertEquals(expectedUrl, driver.getCurrentUrl());
        driver.close();
    }
}

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class HomePageTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void registrationLinkTest() throws InterruptedException {

        String expectedUrl = "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F";

        HomePage homePage = new HomePage(driver, "https://link.springer.com/");
        homePage.clickRegisterLink();
        Thread.sleep(500);

        assertEquals(expectedUrl, driver.getCurrentUrl());
        driver.close();
    }

    @Test
    public void advancedSearchLinkTest() throws InterruptedException {

        String expectedUrl = "https://link.springer.com/advanced-search";

        HomePage homePage = new HomePage(driver, "https://link.springer.com/");
        homePage.clickAdvancedSearchLink();
        Thread.sleep(500);

        assertEquals(expectedUrl, driver.getCurrentUrl());
        driver.close();
    }

}

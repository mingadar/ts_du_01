import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class SignupLoginPageTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void loginTest() throws InterruptedException {

        String email = "drn.mingazova@gmail.com";
        String password = "password123";
        String expectedUrl = "https://link.springer.com/";

        SignupLoginPage signupLoginPage = new SignupLoginPage(driver, "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F");
        signupLoginPage.setLoginEmail(email);
        signupLoginPage.setLoginPassword(password);
        Thread.sleep(500);
        signupLoginPage.sendLoginForm();

        assertEquals(expectedUrl, driver.getCurrentUrl());
        driver.close();
    }
}

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProcessTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterEach
    public void closeWindow() {
        driver.quit();
    }

    @Test
    public void prepareData() throws InterruptedException {
        // Home Page
        HomePage homePage = new HomePage(driver, "https://link.springer.com/");
        homePage.clickAdvancedSearchLink();

        // Advanced Search Page
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver, driver.getCurrentUrl());
        advancedSearchPage.setAllWords("Page Object Model");
        advancedSearchPage.setLeastWords("Sellenium Testing");
        advancedSearchPage.setDateFacetModeToIn();
        advancedSearchPage.setFacetStartYear(String.valueOf(LocalDate.now().getYear()));
        Thread.sleep(2000);
        advancedSearchPage.sendForm();

        // Search Page
        SearchPage searchPage = new SearchPage(driver, driver.getCurrentUrl());
        searchPage.setContentType("Article");

        ArrayList<String> links = searchPage.getResultsLinks(4);

        // Data of articles
        StringBuilder dataForCSV = new StringBuilder();

        for (String link : links) {
            ArticlePage page = new ArticlePage(driver, link);
            dataForCSV.append(page.getName()).append(", ");
            dataForCSV.append(page.getDate()).append(", ");
            dataForCSV.append(page.getDOI()).append("\n");
        }

        // Writing data into csv file
        try {
            FileWriter myWriter = new FileWriter("src/main/resources/articles.csv");
            myWriter.write(dataForCSV.toString());
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/articles.csv")
    public void parameterizedTest(String name, String date, String DOI) throws InterruptedException {

        // Home page
        HomePage homePage = new HomePage(driver, "https://link.springer.com/");
        homePage.clickRegisterLink();

        // Login
        SignupLoginPage signupLoginPage = new SignupLoginPage(driver);
        signupLoginPage.setLoginEmail("drn.mingazova@gmail.com");
        signupLoginPage.setLoginPassword("password123");
        signupLoginPage.sendLoginForm();

        // From home page to search
        homePage.clickAdvancedSearchLink();

        // Search
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.setTitleIsWords(name);
        advancedSearchPage.sendForm();

        // Get
        SearchPage searchPage = new SearchPage(driver);

        // Article Page
        ArticlePage articlePage = new ArticlePage(driver, searchPage.getResultsLinks(1).get(0));
        Thread.sleep(2000);

        assertEquals(name, articlePage.getName());
        assertEquals(date, articlePage.getDate());
        assertEquals(DOI, articlePage.getDOI());
    }
}

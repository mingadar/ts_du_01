package archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PurchasesArchiveTest {

    static PurchasesArchive archive;
    static ArrayList<Order> orders;
    static StandardItem testedItem;

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final ByteArrayOutputStream error = new ByteArrayOutputStream();

    private void streamSetUp() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(error));
    }

    @BeforeEach
    void initArchive() {
        archive = new PurchasesArchive();
        orders = new ArrayList<>();

        //init items
        var jacket = new StandardItem(1, "Jacket", 250.45f, "Clothes", 12);
        var shirt = new StandardItem(2, "Shirt", 130.35f, "Clothes", 10);
        var juice = new StandardItem(3, "Juice", 50.25f, "Beverages", 8);

        //adding items to the cart
        ArrayList<Item> cartItems1 = new ArrayList<>();
        cartItems1.add(jacket);
        cartItems1.add(shirt);
        var cart1 = new ShoppingCart(cartItems1);

        ArrayList<Item> cartItems2 = new ArrayList<>();
        cartItems2.add(juice);
        var cart2 = new ShoppingCart(cartItems2);

        ArrayList<Item> cartItems3 = new ArrayList<>();
        cartItems3.add(jacket);
        cartItems3.add(juice);
        cartItems3.add(shirt);
        var cart3 = new ShoppingCart(cartItems3);

        testedItem = jacket;

        //init orders
        orders.add(new Order(cart1, "Harry Potter", "London, Main Street, 5"));
        orders.add(new Order(cart2, "John Lennon", "Liverpool, Main Street, 1"));
        orders.add(new Order(cart3, "David Beckham", "Washington, Main Street, 2"));

        streamSetUp();
    }

    @AfterEach
    void streamRestore() {
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    void printItemPurchaseStatistics_ValidItemsPassed_CorrectItems() {

        orders.forEach(order -> {
            archive.putOrderToPurchasesArchive(order);
        });

        archive.printItemPurchaseStatistics();

        assertEquals("""
                ITEM PURCHASE STATISTICS:
                ITEM  Item   ID 1   NAME Jacket   CATEGORY Clothes   PRICE 250.45   LOYALTY POINTS 12   HAS BEEN SOLD 2 TIMES
                ITEM  Item   ID 2   NAME Shirt   CATEGORY Clothes   PRICE 130.35   LOYALTY POINTS 10   HAS BEEN SOLD 2 TIMES
                ITEM  Item   ID 3   NAME Juice   CATEGORY Beverages   PRICE 50.25   LOYALTY POINTS 8   HAS BEEN SOLD 2 TIMES
                """, out.toString());
    }

    @Test
    void getHowManyTimesHasBeenItemSold_HowManyTimesItemHasBeenSold_CorrectAmountOfTimes() {

        orders.forEach(order -> {
            archive.putOrderToPurchasesArchive(order);
        });

        assertEquals(2, archive.getHowManyTimesHasBeenItemSold(testedItem));
    }

    @Test
    void putOrderToPurchasesArchive_ValidOrderPassed_CorrectOrder() {

        var order = orders.get(1);
        archive.putOrderToPurchasesArchive(order);

        assertThat(archive.getOrderArchive()).contains(order);
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 7, 90, 12, 578})
    void orderArchive(int amount) {

        var archive = Mockito.mock(PurchasesArchive.class);
        var jacket = new StandardItem(1, "Jacket", 250.45f, "Clothes", 12);

        Mockito.when(archive.getHowManyTimesHasBeenItemSold(jacket)).thenReturn(amount);

        int act = archive.getHowManyTimesHasBeenItemSold(jacket);

        assertEquals(act, amount);
    }

    @Test
    void ItemPurchaseArchiveEntry() {

        var itemPurchaseArchiveEntryMock = Mockito.mock(ItemPurchaseArchiveEntry.class);

        var expected = new StandardItem(1, "Jacket", 250.45f, "Clothes", 12);

        Mockito.when(itemPurchaseArchiveEntryMock.getRefItem()).thenReturn(expected);

        assertEquals(expected, itemPurchaseArchiveEntryMock.getRefItem());
    }

    @Test
    void  ItemPurchaseArchiveEntry_ValidParametersPassed_CorrectInstances() {

        var item = new StandardItem(1, "Jacket", 250.45f, "Clothes", 12);
        var itemPurchaseArchiveEntry  = new ItemPurchaseArchiveEntry(item);

        assertEquals(item, itemPurchaseArchiveEntry.getRefItem());
    }


}
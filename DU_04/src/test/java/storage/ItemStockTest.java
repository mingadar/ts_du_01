package storage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    static ItemStock itemStock;

    @BeforeEach
    void initItemStock() {
        itemStock = new ItemStock(new StandardItem(1, "Banana", 33.33f, "Food", 7));
    }

    @Test
    void ItemStock_ValidParametersPassed_CorrectInstances() {

        assertEquals(new StandardItem(1, "Banana", 33.33f, "Food", 7), itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 67, 23, 12, 21})
    void IncreaseItemCount_ItemsAdded_IncreasedAmount(int amount) {

        int oldAmount = itemStock.getCount();
        itemStock.IncreaseItemCount(amount);

        assertEquals(oldAmount + amount, itemStock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {235, 567, 736, 45, 90})
    void DecreaseItemCount_ItemsRemoved_DecreasedAmount(int amount) {

        int oldAmount = itemStock.getCount();
        itemStock.decreaseItemCount(amount);

        assertEquals(oldAmount - amount, itemStock.getCount());
    }

}
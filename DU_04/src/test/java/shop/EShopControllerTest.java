package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import storage.NoItemInStorage;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EShopControllerTest {
    EShopController ec;
    Item[] storageItems = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
            new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
            new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
            new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
    };


    @BeforeEach
    public void setup() {
        ec = new EShopController();
        int[] itemCount = {10,10,4,5,10,2};

        for (int i = 0; i < storageItems.length; i++) {
            ec.getStorage().insertItems(storageItems[i], itemCount[i]);
        }
    }


    @Test
    public void EshopController_ProcessTestForNormalShopping1_EverythingOK() throws Exception {
        //checking if everything is stored correctly
        assertEquals(10, ec.getStorage().getItemCount(1));
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[4]);
        newCart.addItem(storageItems[5]);

        //checking if all items are in the cart
        assertEquals(5, newCart.getItemsCount());
        assertEquals(12160, newCart.getTotalPrice());

        //checking if item was removed from the cart
        newCart.removeItem(1);
        assertEquals(4, newCart.getItemsCount());

        //checking if we can't remove item which is not in the cart
        assertThrows(Exception.class,
                () -> {
                newCart.removeItem(4);
            });

        assertEquals(7160, newCart.getTotalPrice());

        //when items from the cart were bought -> amount of these items in storage is decreasing
        ec.purchaseShoppingCart(newCart, "Harry Potter", "London, Main Street, 5");
        assertEquals(10, ec.getStorage().getItemCount(1));
        assertEquals(9, ec.getStorage().getItemCount(2));
        assertEquals(3, ec.getStorage().getItemCount(3));
        assertEquals(5, ec.getStorage().getItemCount(4));
        assertEquals(9, ec.getStorage().getItemCount(5));
        assertEquals(1, ec.getStorage().getItemCount(6));

        //archive went well
        assertEquals(4, ec.getArchive().getItemPurchaseArchive().size());
        assertEquals(1, ec.getArchive().getItemPurchaseArchive().get(2).getCountHowManyTimesHasBeenSold());
        assertEquals(1, ec.getArchive().getItemPurchaseArchive().get(3).getCountHowManyTimesHasBeenSold());
        assertEquals(1, ec.getArchive().getItemPurchaseArchive().get(5).getCountHowManyTimesHasBeenSold());
        assertEquals(1, ec.getArchive().getItemPurchaseArchive().get(6).getCountHowManyTimesHasBeenSold());

        //try the same with only one item and preparation for getting Item out of the storage
        ShoppingCart myCart = new ShoppingCart();
        myCart.addItem(storageItems[5]);
        ec.purchaseShoppingCart(myCart, "Name Surname", "New York, Wall Street, 1");
        assertEquals(0, ec.getStorage().getItemCount(6));
        assertEquals(2, ec.getArchive().getItemPurchaseArchive().get(6).getCountHowManyTimesHasBeenSold());

        //item is not in the storage
        assertThrows(NoItemInStorage.class,
                () -> {
                ec.purchaseShoppingCart(myCart, "Name Surname", "New York, Wall Street, 1");
        });

    }


    @Test
    public void EshopController_ProcessTestForNormalShopping2_EverythingOK() throws Exception {
        //checking if everything is stored correctly
        assertEquals(10, ec.getStorage().getItemCount(5));
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(storageItems[3]);
        newCart.addItem(storageItems[5]);

        //checking if all items are in the cart
        assertEquals(2, newCart.getItemsCount());
        assertEquals(1070, newCart.getTotalPrice());

        //another items added
        newCart.addItem(storageItems[3]);
        newCart.addItem(storageItems[3]);
        newCart.addItem(storageItems[3]);
        assertEquals(5, newCart.getItemsCount());
        assertEquals(2120, newCart.getTotalPrice());

        //checking if item is correctly removed from the cart
        newCart.removeItem(6);
        assertEquals(4, newCart.getItemsCount());

        //can't remove item which is not in the cart
        assertThrows(Exception.class,
                () -> {
                newCart.removeItem(5);
        });

        assertEquals(1400, newCart.getTotalPrice());

        //when items from the cart were bought -> amount of these items in storage is decreasing
        ec.purchaseShoppingCart(newCart, "Harry Potter", "London, Main Street, 1");
        assertEquals(10, ec.getStorage().getItemCount(1));
        assertEquals(10, ec.getStorage().getItemCount(2));
        assertEquals(4, ec.getStorage().getItemCount(3));
        assertEquals(1, ec.getStorage().getItemCount(4));
        assertEquals(10, ec.getStorage().getItemCount(5));
        assertEquals(2, ec.getStorage().getItemCount(6));

        //archive went well
        assertEquals(1, ec.getArchive().getItemPurchaseArchive().size());
        assertEquals(4, ec.getArchive().getItemPurchaseArchive().get(4).getCountHowManyTimesHasBeenSold());

        //try the same with only one item and preparation for getting Item out of the storage
        ShoppingCart myCart = new ShoppingCart();
        myCart.addItem(storageItems[3]);
        ec.purchaseShoppingCart(myCart, "Name Surname", "New York, Wall Street, 1");
        assertEquals(0, ec.getStorage().getItemCount(4));
        assertEquals(5, ec.getArchive().getItemPurchaseArchive().get(4).getCountHowManyTimesHasBeenSold());

        //item is not in the storage
        assertThrows(NoItemInStorage.class, () -> {ec.purchaseShoppingCart(myCart, "Name Surname", "New York, Wall Street, 1");});

    }
}

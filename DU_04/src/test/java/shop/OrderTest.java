package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    static ShoppingCart cart;

    @BeforeEach
    void initCart() {
        cart = new ShoppingCart();
    }

    @Test
    void OrderFirstConstructor_ValidParametersPassed_CorrectInstances() {

       String customerName = "John Smith";
       String customerAddress = "Prague, Main Street, 3";

       cart.addItem(new StandardItem(1, "Jacket", 250.45f, "Clothes", 12));

       var order = new Order(cart, customerName, customerAddress, 1);

       assertEquals(cart.getCartItems(), order.getItems());
       assertEquals(customerName, order.getCustomerName());
       assertEquals(customerAddress, order.getCustomerAddress());
       assertEquals(1, order.getState());
    }

    @Test
    void OrderSecondConstructor_ValidParametersPassed_CorrectInstances() {

        String customerName = "John Lennon";
        String customerAddress = "Liverpool, Main Street, 1";

        cart.addItem(new StandardItem(2, "Guitar", 3500.99f, "Music", 15));

        var order = new Order(cart, customerName, customerAddress);

        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
    void Order_CartIsNull_ExceptionThrown() {

        assertThrows(NullPointerException.class,
                () -> {
                    new Order(null, "David Beckham", "Washington, Main Street, 2", 1);
                });
        assertThrows(NullPointerException.class,
                () -> {
                    new Order(null, "Harry Potter", "London, Main Street, 5");
                });
    }
}
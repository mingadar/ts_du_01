package shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @Test
    void StandardItem_ValidParametersPassed_CorrectInstances() {

        var standardItem = new StandardItem(1, "Jacket", 250.45f, "Clothes", 12);

        //Checking ID
        assertEquals(1, standardItem.getID());

        //Checking name
        assertEquals("Jacket", standardItem.getName());

        //Checking price
        assertEquals(250.45f, standardItem.getPrice());

        //Checking category
        assertEquals("Clothes", standardItem.getCategory());

        //Checking loyalty points
        assertEquals(12, standardItem.getLoyaltyPoints());
    }

    @Test
    void Copy_ValidParametersAreCopied_ItemsEqual() {

        var standardItem = new StandardItem(1, "Jacket", 250.45f, "Clothes", 12);
        var copiedItem = standardItem.copy();

        assertEquals(standardItem, copiedItem);
    }

    @ParameterizedTest
    @CsvSource(value = {"1, Jacket, 250.45f, Clothes, 12", "2, Shirt, 120.35f, Clothes, 10", "3, Juice, 50.25f, Beverages, 8"})

    void equals_CompareObj1AndObj2_TheyAreEqual(int id, String name, float price, String category, int loyaltyPoints) {

        var standardItem1 = new StandardItem(id, name, price, category, loyaltyPoints);
        var standardItem2 = new StandardItem(id, name, price, category, loyaltyPoints);
        var standardItemIncorrect = new StandardItem(id + 2, name, price, category, loyaltyPoints);

        assertEquals(standardItem1, standardItem2);
        assertNotSame(standardItem1, standardItemIncorrect);
    }
}